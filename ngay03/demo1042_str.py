s='abcdefghijk'
#  012345

print(s)
print(s[0] + ' tai vitri so 0')
print(s[1] + ' tai vitri so 1')
print(s[5] + ' tai vitri so 5')
# print(s[6])  # IndexError: string index out of range

print( len(s) )

i = 10
print(s[i] + f' tai vitri so {i}')
